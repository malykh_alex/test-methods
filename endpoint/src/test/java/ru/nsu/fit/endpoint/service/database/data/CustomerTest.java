package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.shared.JsonMapper;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */

/**
 * Тестирование создания объекта Customer
 * 1. Тестирование ошибок, связанных с полем firstName
 *
 *  1.1 Имя слишком короткое
 *      1.1.1   Создать Customer с именем из одной прописной буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.1.2   Создать Customer с именем из двух букв
 *          а)  Проверить отсутсвие ошибки
 *
 *  1.2 Имя слишком длинное
 *      1.2.1   Создать Customer с именем из 13 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.2.2   Создать Customer с именем из 12 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить отсутсвие ошибки
 *
 *  1.3 Имя не соответствует паттерну: имя должно начинаться с прописной буквы, остальные символы должно быть строчными,
 *      других символов быть не должно.
 *      1.3.1   Создать Customer с именем, начинающимся с маленькой буквы, остальные буквы должны быть строчными
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.3.2   Создать Customer с именем, содержащим другие прописные буквы, кроме первой
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.3.3   Создать Customer с именем, содержащим не только буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 *  1.4 Имя равно null
 *      1.4.1   Создать Customer с именем равным null
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 * 2. Тестирование ошибок, связанных с полем lastName
 *
 *  2.1 Фамилия слишком короткая
 *      2.1.1   Создать Customer с фамилией из одной прописной буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.1.2   Создать Customer с фамилией из двух букв
 *          а)  Проверить отсутсвие ошибки
 *
 *  2.2 Фамилия слишком длинная
 *      2.2.1   Создать Customer с фамилией из 13 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.2.2   Создать Customer с фамилией из 12 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить отсутсвие ошибки
 *
 *  2.3 Фамилия не соответствует паттерну: фамилия должна начинаться с прописной буквы, остальные символы должно быть строчными,
 *      других символов быть не должно.
 *      2.3.1   Создать Customer с фамилией, начинающимся с маленькой буквы, остальные буквы должны быть строчными
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.3.2   Создать Customer с фамилией, содержащим другие прописные буквы, кроме первой
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.3.3   Создать Customer с фамилией, содержащим не только буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 *  2.4 Фамилия равна null
 *      2.4.1   Создать Customer с фамилией равной null
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 * 3. Тестирование того, что login является e-mail
 *
 *  3.1 Логин содержить символ @
 *      3.1.1   Создать Customer с login не содержащим символ @
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  3.2 Логин содержит доменное имя
 *      3.1.2   Создать Customer, не содержащий доменного имени
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  3.3 Логин не начинается с доменного имени
 *      3.1.3   Создать Customer, начинающийся с символа @
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  3.4 логин не является null
 *      3.1.4   Создать Customer, с логином равным null
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 * 4. Тестирование ошибок, связанных с полем password
 *
 *  4.1 Пароль слишком короткий
 *      4.1.1   Создать Customer с паролем, состоящим из 5 символов
 *          a)  Проверить наличие ошибки и корректного сообщения
 *      4.1.2   Создать Customer с паролем, состоящим из 6 символов
 *          a)  Проверить отсутствие ошибки
 *
 *  4.2 Пароль слишком длинный
 *      4.2.1   Создать Customer с паролем, состоящим из 13 символов
 *          a)  Проверить наличие ошибки и корректного сообщения
 *      4.2.2   Создать Customer с паролем, состоящим из 12 символов
 *          a)  Проверить отсутствие ошибки
 *
 *  4.3 Пароль не содержит имя
 *      4.3.1   Создать Customer с паролем, с именем в качестве подстроки
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.4 Пароль не содержит фамилия
 *      4.4.1   Создать Customer с паролем, с фамилией в качестве подстроки
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.5 Пароль не содержит логин
 *      4.5.1   Создать Customer с паролем, с логином в качестве подстроки
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.6 Пароль содержит буквы
 *      4.6.1   Создать Customer с паролем, не содержащим буквы
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.7 Пароль содержит цифры
 *      4.7.1   Создать Customer с паролем, не содержащим цифр
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.8 Пароль не является null
 *      4.8.1   Создать Customer с паролем раным null
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 * 5. Тестирование ошибок, связанных с полем balance
 *
 *  5.1 Баланс не может быть отрицательным
 *      5.1.1   Создать Customer с отрицательным балансом
 *          a)  Проверить наличие ошибки и корректного сообщения
 *      5.1.2   Создать Customer с нулевым балансом
 *          a)  Проверить наличие ошибки и корректного сообщения
 */

public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    //Correct tests
    @Test
    public void testCreateCustomerWithEverythingCorrect1() {

/*        String s = "{\"firstName\" : \"Vasya\", \"lastName\" : \"Pupkin\", \"login\" : \"azaza@mail.ru\", \"pswd\" : \"ataTa123\", \"money\" : \"1000\"}";
        JsonMapper.fromJson(s, Customer.CustomerData.class);*/

        new Customer.CustomerData("Bu", "Jo", "buba@nsu.ru", "39sds1", 0);
    }

    @Test
    public void testCreateCustomerWithEverythingCorrect2() {
        new Customer.CustomerData("Dubeurkabost", "Smirnovivano", "buba@nsu.ru", "39sds1211111", 0);
    }

    //First name tests =================================================================
    @Test
    public void testCreateCustomerWithShortFirstName() {
        expectedEx.expect(InvalidCustomerFirstNameException.class);
        expectedEx.expectMessage(InvalidCustomerFirstNameException.EX_FNAME_TOO_SHORT);
        new Customer.CustomerData("W", "Joe", "buba@nsu.ru", "39sds12", 0);
    }

    @Test
    public void testCreateCustomerWithLongFirstName() {
        expectedEx.expect(InvalidCustomerFirstNameException.class);
        expectedEx.expectMessage(InvalidCustomerFirstNameException.EX_FNAME_TOO_LONG);
        new Customer.CustomerData("Dubeurkaboste", "Joe", "buba@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateCustomerWithFirstNameStartsWithNonCipital() {
        expectedEx.expect(InvalidCustomerFirstNameException.class);
        expectedEx.expectMessage(InvalidCustomerFirstNameException.EX_FNAME_PATTERM);
        new Customer.CustomerData("mikhail", "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateCustomerWithFirstNameWithOtherCapitals() {
        expectedEx.expect(InvalidCustomerFirstNameException.class);
        expectedEx.expectMessage(InvalidCustomerFirstNameException.EX_FNAME_PATTERM);
        new Customer.CustomerData("MIKHAIL", "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateCustomerWithFirstNameWithOtherSymbols() {
        expectedEx.expect(InvalidCustomerFirstNameException.class);
        expectedEx.expectMessage(InvalidCustomerFirstNameException.EX_FNAME_PATTERM);
        new Customer.CustomerData("Mikhail11", "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateCustomerWithNullAsFirstName() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidCustomerFirstNameException.EX_FNAME_NPE);
        new Customer.CustomerData(null, "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }


    //Last name tests =================================================================
    @Test
    public void testCreateCustomerWithShortLastName() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(InvalidCustomerLastNameException.EX_LNAME_TOO_SHORT);
        new Customer.CustomerData("Walter", "G", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateCustomerWithLongLastName() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(InvalidCustomerLastNameException.EX_LNAME_TOO_LONG);
        new Customer.CustomerData("Walter", "Smirnovivanov", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateCustomerWithLastNameStartsWithNonCipital() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(InvalidCustomerLastNameException.EX_LNAME_PATTERM);
        new Customer.CustomerData("Walter", "ivanov", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateCustomerWithLastNameWithOtherCapitals() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(InvalidCustomerLastNameException.EX_LNAME_PATTERM);
        new Customer.CustomerData("Walter", "IvanovPetrov", "gook@nsu.ru", "walt1223", 331);
    }

    @Test
    public void testCreateCustomerWithLastNameStartsWithOtherSymbols() {
        expectedEx.expect(InvalidCustomerLastNameException.class);
        expectedEx.expectMessage(InvalidCustomerLastNameException.EX_LNAME_PATTERM);
        new Customer.CustomerData("Walter", "Ivanov777", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateCustomerWithNullAsLastName() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidCustomerLastNameException.EX_LNAME_NPE);
        new Customer.CustomerData("Walter", null, "gook@nsu.ru", "walt1223", 331);
    }

    //Login tests =================================================================
    @Test
    public void testCreateCustomerWithLoginWithoutAtSymbol() {
        expectedEx.expect(InvalidCustomerLoginException.class);
        expectedEx.expectMessage(InvalidCustomerLoginException.EX_LOGIN_PATTERN);
        new Customer.CustomerData("Walter", "Danilov", "gooknsu.ru", "walt1223", 331);
    }

    @Test
    public void testCreateCustomerWithLoginWithoutDomain() {
        expectedEx.expect(InvalidCustomerLoginException.class);
        expectedEx.expectMessage(InvalidCustomerLoginException.EX_LOGIN_PATTERN);
        new Customer.CustomerData("Walter", "Danilov", "walt@", "walt1223", 331);
    }

    @Test
    public void testCreateCustomerWithLoginStartsWithAtSymbol() {
        expectedEx.expect(InvalidCustomerLoginException.class);
        expectedEx.expectMessage(InvalidCustomerLoginException.EX_LOGIN_PATTERN);
        new Customer.CustomerData("Walter", "Danilov", "@nsu.ru", "walt1223", 331);
    }

    @Test
    public void testCreateCustomerWithNullAsLogin() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidCustomerLoginException.EX_LOGIN_NPE);
        new Customer.CustomerData("Walter", "Danilov", null, "walt1223", 331);
    }

    //Password tests =================================================================
    @Test
    public void testCreateCustomerWithTooShortPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_TOO_SHORT);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "bsu42", 200);
    }

    @Test
    public void testCreateCustomerWithTooLongPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_TOO_LONG);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "veryreliable7", 200);
    }

    @Test
    public void testCreateCustomerWithFirstNameAsPartOfPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_CONT_FNAME);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "walter12", 0);
    }

    @Test
    public void testCreateCustomerWithLastNameAsPartOfPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_CONT_LNAME);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "6DaniloV1", 0);
    }

    @Test
    public void testCreateCustomerWithLoginAsPartOfPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_CONT_LOGIN);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "hhok@nsu.ru6", 0);
    }

    @Test
    public void testCreateCustomerWithNoAlphabeticsInPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_ONE_ALPH);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "57458439", 0);
    }

    @Test
    public void testCreateCustomerWithNoDigitsInPassword() {
        expectedEx.expect(InvalidCustomerPasswordException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_ONE_DIGIT);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "GoodPassword", 0);
    }

    @Test
    public void testCreateCustomerWithNullAsPassword() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidCustomerPasswordException.EX_PSWD_NPE);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", null, 0);
    }

    //Balance tests =================================================================
    @Test
    public void testCreateCustomerWithNegativeBalance0() {
        expectedEx.expect(InvalidCustomerBalanceException.class);
        expectedEx.expectMessage(InvalidCustomerBalanceException.EX_BAL_NEG);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "pswd13", -1);
    }

    @Test
    public void testCreateCustomerWithNegativeBalance1() {
        expectedEx.expect(InvalidCustomerBalanceException.class);
        expectedEx.expectMessage(InvalidCustomerBalanceException.EX_BAL_NEG);
        new Customer.CustomerData("Walter", "Danilov", "hhok@nsu.ru", "pswd13", -8938223);
    }

}
