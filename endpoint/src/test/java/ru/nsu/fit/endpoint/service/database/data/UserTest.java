package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * Created by Alex on 30.09.2016.
 */

/**
 * Тестирование создания объекта User
 * 1. Тестирование ошибок, связанных с полем firstName
 *
 *  1.1 Имя слишком короткое
 *      1.1.1   Создать User с именем из одной прописной буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.1.2   Создать User с именем из двух букв
 *          а)  Проверить отсутсвие ошибки
 *
 *  1.2 Имя слишком длинное
 *      1.2.1   Создать User с именем из 13 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.2.2   Создать User с именем из 12 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить отсутсвие ошибки
 *
 *  1.3 Имя не соответствует паттерну: имя должно начинаться с прописной буквы, остальные символы должно быть строчными,
 *      других символов быть не должно.
 *      1.3.1   Создать User с именем, начинающимся с маленькой буквы, остальные буквы должны быть строчными
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.3.2   Создать User с именем, содержащим другие прописные буквы, кроме первой
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      1.3.3   Создать User с именем, содержащим не только буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 *  1.4 Имя равно null
 *      1.4.1   Создать User с именем равным null
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 * 2. Тестирование ошибок, связанных с полем lastName
 *
 *  2.1 Фамилия слишком короткая
 *      2.1.1   Создать User с фамилией из одной прописной буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.1.2   Создать User с фамилией из двух букв
 *          а)  Проверить отсутсвие ошибки
 *
 *  2.2 Фамилия слишком длинная
 *      2.2.1   Создать User с фамилией из 13 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.2.2   Создать User с фамилией из 12 букв, удовлетворяющих остальным критериям имени
 *          а)  Проверить отсутсвие ошибки
 *
 *  2.3 Фамилия не соответствует паттерну: фамилия должна начинаться с прописной буквы, остальные символы должно быть строчными,
 *      других символов быть не должно.
 *      2.3.1   Создать User с фамилией, начинающимся с маленькой буквы, остальные буквы должны быть строчными
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.3.2   Создать User с фамилией, содержащим другие прописные буквы, кроме первой
 *          а)  Проверить наличие ошибки и корректного сообщения
 *      2.3.3   Создать User с фамилией, содержащим не только буквы
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 *  2.4 Фамилия равна null
 *      2.4.1   Создать User с фамилией равной null
 *          а)  Проверить наличие ошибки и корректного сообщения
 *
 * 3. Тестирование того, что login является e-mail
 *
 *  3.1 Логин содержить символ @
 *      3.1.1   Создать User с login не содержащим символ @
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  3.2 Логин содержит доменное имя
 *      3.1.2   Создать User, не содержащий доменного имени
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  3.3 Логин не начинается с доменного имени
 *      3.1.3   Создать User, начинающийся с символа @
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  3.4 логин не является null
 *      3.1.4   Создать User, с логином равным null
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 * 4. Тестирование ошибок, связанных с полем password
 *
 *  4.1 Пароль слишком короткий
 *      4.1.1   Создать User с паролем, состоящим из 5 символов
 *          a)  Проверить наличие ошибки и корректного сообщения
 *      4.1.2   Создать User с паролем, состоящим из 6 символов
 *          a)  Проверить отсутствие ошибки
 *
 *  4.2 Пароль слишком длинный
 *      4.2.1   Создать User с паролем, состоящим из 13 символов
 *          a)  Проверить наличие ошибки и корректного сообщения
 *      4.2.2   Создать User с паролем, состоящим из 12 символов
 *          a)  Проверить отсутствие ошибки
 *
 *  4.3 Пароль не содержит имя
 *      4.3.1   Создать User с паролем, с именем в качестве подстроки
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.4 Пароль не содержит фамилия
 *      4.4.1   Создать User с паролем, с фамилией в качестве подстроки
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.5 Пароль не содержит логин
 *      4.5.1   Создать User с паролем, с логином в качестве подстроки
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.6 Пароль содержит буквы
 *      4.6.1   Создать User с паролем, не содержащим буквы
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.7 Пароль содержит цифры
 *      4.7.1   Создать User с паролем, не содержащим цифр
 *          a)  Проверить наличие ошибки и корректного сообщения
 *
 *  4.8 Пароль не является null
 *      4.8.1   Создать User с паролем раным null
 *          a)  Проверить наличие ошибки и корректного сообщения
 */

public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    //Correct tests
    @Test
    public void testCreateUserWithEverythingCorrect1() {
        new User.UserData("Bu", "Jo", "buba@nsu.ru", "39sds1", 0);
    }

    @Test
    public void testCreateUserWithEverythingCorrect2() {
        new User.UserData("Dubeurkabost", "Smirnovivano", "buba@nsu.ru", "39sds1211111", 0);
    }

    //First name tests =================================================================
    @Test
    public void testCreateUserWithShortFirstName() {
        expectedEx.expect(InvalidUserFirstNameException.class);
        expectedEx.expectMessage(InvalidUserFirstNameException.EX_FNAME_TOO_SHORT);
        new User.UserData("W", "Joe", "buba@nsu.ru", "39sds12", 0);
    }

    @Test
    public void testCreateUserWithLongFirstName() {
        expectedEx.expect(InvalidUserFirstNameException.class);
        expectedEx.expectMessage(InvalidUserFirstNameException.EX_FNAME_TOO_LONG);
        new User.UserData("Dubeurkaboste", "Joe", "buba@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateUserWithFirstNameStartsWithNonCipital() {
        expectedEx.expect(InvalidUserFirstNameException.class);
        expectedEx.expectMessage(InvalidUserFirstNameException.EX_FNAME_PATTERM);
        new User.UserData("mikhail", "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateUserWithFirstNameWithOtherCapitals() {
        expectedEx.expect(InvalidUserFirstNameException.class);
        expectedEx.expectMessage(InvalidUserFirstNameException.EX_FNAME_PATTERM);
        new User.UserData("MIKHAIL", "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateUserWithFirstNameWithOtherSymbols() {
        expectedEx.expect(InvalidUserFirstNameException.class);
        expectedEx.expectMessage(InvalidUserFirstNameException.EX_FNAME_PATTERM);
        new User.UserData("Mikhail11", "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }

    @Test
    public void testCreateUserWithNullAsFirstName() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidUserFirstNameException.EX_FNAME_NPE);
        new User.UserData(null, "Mandrik", "mandrik@nsu.ru", "39sds14", 0);
    }


    //Last name tests =================================================================
    @Test
    public void testCreateUserWithShortLastName() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(InvalidUserLastNameException.EX_LNAME_TOO_SHORT);
        new User.UserData("Walter", "G", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateUserWithLongLastName() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(InvalidUserLastNameException.EX_LNAME_TOO_LONG);
        new User.UserData("Walter", "Smirnovivanov", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateUserWithLastNameStartsWithNonCipital() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(InvalidUserLastNameException.EX_LNAME_PATTERM);
        new User.UserData("Walter", "ivanov", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateUserWithLastNameWithOtherCapitals() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(InvalidUserLastNameException.EX_LNAME_PATTERM);
        new User.UserData("Walter", "IvanovPetrov", "gook@nsu.ru", "walt1223", 331);
    }

    @Test
    public void testCreateUserWithLastNameStartsWithOtherSymbols() {
        expectedEx.expect(InvalidUserLastNameException.class);
        expectedEx.expectMessage(InvalidUserLastNameException.EX_LNAME_PATTERM);
        new User.UserData("Walter", "Ivanov777", "rook@nsu.ru", "walt1223", 450);
    }

    @Test
    public void testCreateUserWithNullAsLastName() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidUserLastNameException.EX_LNAME_NPE);
        new User.UserData("Walter", null, "gook@nsu.ru", "walt1223", 331);
    }

    //Login tests =================================================================
    @Test
    public void testCreateUserWithLoginWithoutAtSymbol() {
        expectedEx.expect(InvalidUserLoginException.class);
        expectedEx.expectMessage(InvalidUserLoginException.EX_LOGIN_PATTERN);
        new User.UserData("Walter", "Danilov", "gooknsu.ru", "walt1223", 331);
    }

    @Test
    public void testCreateUserWithLoginWithoutDomain() {
        expectedEx.expect(InvalidUserLoginException.class);
        expectedEx.expectMessage(InvalidUserLoginException.EX_LOGIN_PATTERN);
        new User.UserData("Walter", "Danilov", "walt@", "walt1223", 331);
    }

    @Test
    public void testCreateUserWithLoginStartsWithAtSymbol() {
        expectedEx.expect(InvalidUserLoginException.class);
        expectedEx.expectMessage(InvalidUserLoginException.EX_LOGIN_PATTERN);
        new User.UserData("Walter", "Danilov", "@nsu.ru", "walt1223", 331);
    }

    @Test
    public void testCreateUserWithNullAsLogin() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidUserLoginException.EX_LOGIN_NPE);
        new User.UserData("Walter", "Danilov", null, "walt1223", 331);
    }

    //Password tests =================================================================
    @Test
    public void testCreateUserWithTooShortPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_TOO_SHORT);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "bsu42", 200);
    }

    @Test
    public void testCreateUserWithTooLongPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_TOO_LONG);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "veryreliable7", 200);
    }

    @Test
    public void testCreateUserWithFirstNameAsPartOfPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_CONT_FNAME);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "walter12", 0);
    }

    @Test
    public void testCreateUserWithLastNameAsPartOfPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_CONT_LNAME);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "6DaniloV1", 0);
    }

    @Test
    public void testCreateUserWithLoginAsPartOfPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_CONT_LOGIN);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "hhok@nsu.ru6", 0);
    }

    @Test
    public void testCreateUserWithNoAlphabeticsInPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_ONE_ALPH);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "57458439", 0);
    }

    @Test
    public void testCreateUserWithNoDigitsInPassword() {
        expectedEx.expect(InvalidUserPasswordException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_ONE_DIGIT);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", "GoodPassword", 0);
    }

    @Test
    public void testCreateUserWithNullAsPassword() {
        expectedEx.expect(NullPointerException.class);
        expectedEx.expectMessage(InvalidUserPasswordException.EX_PSWD_NPE);
        new User.UserData("Walter", "Danilov", "hhok@nsu.ru", null, 0);
    }

}