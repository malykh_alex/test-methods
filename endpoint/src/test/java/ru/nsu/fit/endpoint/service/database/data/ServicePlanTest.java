package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.exception.*;

/**
 * Created by Eugene on 03.10.2016.
 */

/**
 * Тестирование класса ServicePlan:
 *
 * 1. Тестирование ошибок с полем name.
 *
 *      1.1. Поле слишком короткое.
 *          1.1.1. Создать ServicePlan с полем name, состоящее из одной буквы.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      1.2. Поле слишком длинное.
 *          1.2.1. Создать ServicePlan с полем name, состоящее из ста двадцати девяти символов.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      1.3. Поле не соответствует шаблону.
 *          1.3.1. Создать ServicePlan с полем name, содержащим спецсимволы.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *
 * 2. Тестирование ошибок с полем details.
 *
 *      2.1. Поле пусто.
 *          2.1.1. Создать ServicePlan с полем details, равным null.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      2.2. Поле слишком длинно.
 *          2.2.1. Создать ServicePlan с полем details в тысчу двести двадцать пять символов.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      2.3. Поле слишком короткое.
 *          2.2.1. Создать ServicePlan с пустым полем details.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *
 * 3. Тестирование ошибок с полем maxSeats.
 *
 *      3.1. Значение поля меньше минимально дозволенного.
 *          3.1.1. Создать ServicePlan с полем maxSeats, равным нулю.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      3.2. Значение поля больше максимально дозволенного.
 *          3.2.1.  Создать ServicePlan с полем maxSeats, равным одному миллиону.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *
 * 4. Тестирование ошибок с полем minSeats.
 *
 * 4.1. Значение поля меньше минимально дозволенного.
 *          4.1.1. Создать ServicePlan с полем minSeats, равным нулю.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      4.2. Значение поля больше максимально дозволенного.
 *          4.2.1.  Создать ServicePlan с полем minSeats, равным одному миллиону.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *
 * 5. Тестирование ошибок с полем feePerUnit.
 *
 * 5.1. Значение поля меньше минимально дозволенного.
 *          5.1.1. Создать ServicePlan с полем feePerUnit, равным минус единице.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *      5.2. Значение поля больше максимально дозволенного.
 *          5.2.1.  Создать ServicePlan с полем feePerUnit, равным одному миллиону.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *
 * 6. Тестирование соблюдения зависимости между  maxSeats и minSeats.
 *           6.1.1. Создание ServicePlan с полем maxSeats меньшим minSeats.
 *              a) Проверить наличие ошибки и соответствующего сообщения.
 *
 */

public class ServicePlanTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testShortName() {
        expectedException.expect(InvalidPlanNameException.class);
        expectedException.expectMessage(InvalidPlanNameException.EX_NAME_TOO_SHORT);
        new Plan.PlanData("", "Smth", 1, 1, 1);
    }

    @Test
    public void testLongName() {
        expectedException.expect(InvalidPlanNameException.class);
        expectedException.expectMessage(InvalidPlanNameException.EX_NAME_TOO_LONG);
        new Plan.PlanData("John DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn Doee", "Smth", 1, 1, 1);
    }

    @Test
    public void testInvalidName() {
        expectedException.expect(InvalidPlanNameException.class);
        expectedException.expectMessage(InvalidPlanNameException.EX_NAME_IS_INVALID);
        new Plan.PlanData("John Doe!", "Smth", 1, 1, 1);
    }

    @Test
    public void testNullDetails() {
        expectedException.expect(InvalidPlanDetailsException.class);
        expectedException.expectMessage(InvalidPlanDetailsException.EX_DETAILS_IS_NULL);
        new Plan.PlanData("John Doe", null, 1, 1, 1);
    }

    @Test
    public void testLongDetails() {
        expectedException.expect(InvalidPlanDetailsException.class);
        expectedException.expectMessage(InvalidPlanDetailsException.EX_DETAILS_TOO_LONG);
        new Plan.PlanData("John Doe", "John DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn DoeJohn Doee", 1, 1, 1);
    }

    @Test
    public void testShortDetails() {
        expectedException.expect(InvalidPlanDetailsException.class);
        expectedException.expectMessage(InvalidPlanDetailsException.EX_DETAILS_TOO_SHORT);
        new Plan.PlanData("John Doe", "", 1, 1, 1);
    }

    @Test
    public void testSmallMaxSeats() {
        expectedException.expect(InvalidPlanMaxSeatsException.class);
        expectedException.expectMessage(InvalidPlanMaxSeatsException.EX_MAX_SEATS_TOO_SMALL);
        new Plan.PlanData("John Doe", "Smth", 0, 0, 1);
    }

    @Test
    public void testLargeMaxSeats() {
        expectedException.expect(InvalidPlanMaxSeatsException.class);
        expectedException.expectMessage(InvalidPlanMaxSeatsException.EX_MAX_SEATS_TOO_LARGE);
        new Plan.PlanData("John Doe", "Smth", 1000000, 0, 1);
    }

    @Test
    public void testSmallMinSeats() {
        expectedException.expect(InvalidPlanMinSeatsException.class);
        expectedException.expectMessage(InvalidPlanMinSeatsException.EX_MIN_SEATS_TOO_SMALL);
        new Plan.PlanData("John Doe", "Smth", 1, 0, 1);
    }

    @Test
    public void testLargeMinSeats() {
        expectedException.expect(InvalidPlanMinSeatsException.class);
        expectedException.expectMessage(InvalidPlanMinSeatsException.EX_MIN_SEATS_TOO_LARGE);
        new Plan.PlanData("John Doe", "Smth", 1, 1000000, 1);
    }

    @Test
    public void testLowFeePerUnit() {
        expectedException.expect(InvalidPlanFeePerUnitException.class);
        expectedException.expectMessage(InvalidPlanFeePerUnitException.EX_FEEPERUNIT_TOO_SMALL);
        new Plan.PlanData("John Doe", "Smth", 1, 1, -1);
    }

    @Test
    public void testLargeFeePerUnit() {
        expectedException.expect(InvalidPlanFeePerUnitException.class);
        expectedException.expectMessage(InvalidPlanFeePerUnitException.EX_FEEPERUNIT_TOO_LARGE);
        new Plan.PlanData("John Doe", "Smth", 1, 1, 1000000);
    }

    @Test
    public void testMaxSeatsMinSeats() {
        expectedException.expect(InvalidPlanMaxSeatsException.class);
        expectedException.expectMessage(InvalidPlanMaxSeatsException.EX_MAX_SEATS_TOO_SMALL);
        new Plan.PlanData("John Doe", "Smth", 1, 2, 1);
    }
}
