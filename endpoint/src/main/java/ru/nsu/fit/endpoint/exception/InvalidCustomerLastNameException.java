package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class InvalidCustomerLastNameException extends CustomerException {

    public static final String EX_LNAME_TOO_SHORT = "Last name is too short (must be at least 2 chars)";
    public static final String EX_LNAME_TOO_LONG = "Last name is too long (must be at most 12 chars)";
    public static final String EX_LNAME_NPE = "Last name is null pointer";
    public static final String EX_LNAME_PATTERM = "Last name does not start with the capital, contain other non-capitals or non alphabetics";

    public InvalidCustomerLastNameException() {
        super();
    }

    public InvalidCustomerLastNameException(String msg) {
        super(msg);
    }

}
