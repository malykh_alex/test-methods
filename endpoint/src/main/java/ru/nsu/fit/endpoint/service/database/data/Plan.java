package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.fit.endpoint.exception.*;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan extends Entity<Plan.PlanData> {

    public UUID getId() {
        return id;
    }

    private UUID id;
    public Plan(PlanData data, UUID id){
        super(data);
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PlanData {

        private PlanData(){}

        private static final int MAX_NAME_LENGTH = 128;
        private static final int MIN_NAME_LENGTH = 2;
        private static final int MAX_DETAILS_LENGTH = 1024;
        private static final int MIN_DETAILS_LENGTH = 1;
        private static final int MAX_MAXSEATS = 999999;
        private static final int MIN_MAXSEATS = 1;
        private static final int MAX_MINSEATS = 999999;
        private static final int MIN_MINSEATS = 1;
        private static final int MAX_FEEPERUNIT = 999999;
        private static final int MIN_FEEPERUNIT = 0;
        private static final String NAME_PATTERN = "^[a-zA-Z ]+[a-zA-Z0-9 ]*$";

        /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
        @JsonProperty("name")
        private String name;

        /* Длина не больше 1024 символов и не меньше 1 включительно */
        @JsonProperty("details")
        private String details;

        /* Не больше 999999 и не меньше 1 включительно */
        @JsonProperty("maxSeats")
        private int maxSeats;

        /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
        @JsonProperty("minSeats")
        private int minSeats;

        /* Больше ли равно 0 но меньше либо равно 999999 */
        @JsonProperty("feePerUnit")
        private int feePerUnit;

        public int getCost() {
            return cost;
        }

        public void setCost(int cost) {
            this.cost = cost;
        }

        @JsonProperty("cost")
        private int cost;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public int getMaxSeats() {
            return maxSeats;
        }

        public void setMaxSeats(int maxSeats) {
            this.maxSeats = maxSeats;
        }

        public int getMinSeats() {
            return minSeats;
        }

        public void setMinSeats(int minSeats) {
            this.minSeats = minSeats;
        }

        public int getFeePerUnit() {
            return feePerUnit;
        }

        public void setFeePerUnit(int feePerUnit) {
            this.feePerUnit = feePerUnit;
        }

        public PlanData(String name, String details, int maxSeats, int minSeats, int feePerUnit) throws PlanException {
            validate(name, details, maxSeats, minSeats, feePerUnit);
            this.name = name;
            this.details = details;
            this.maxSeats = maxSeats;
            this.minSeats = minSeats;
            this.feePerUnit = feePerUnit;
        }


        public PlanData(String name, String details, int maxSeats, int minSeats, int feePerUnit, int cost) throws PlanException {
            validate(name, details, maxSeats, minSeats, feePerUnit);
            this.name = name;
            this.details = details;
            this.maxSeats = maxSeats;
            this.minSeats = minSeats;
            this.feePerUnit = feePerUnit;
            this.cost = cost;
        }

        public void validate(String name, String details, int maxSeats, int minSeats, int feePerUnit) throws PlanException {
            validateName(name);
            validateDetails(details);
            validateMaxSeats(maxSeats);
            validateMinSeats(minSeats);
            validateFeePerUnit(feePerUnit);
            validateMaxSeatsMinSeats(maxSeats, minSeats);
        }

        private void validateMaxSeatsMinSeats(int maxSeats, int minSeats) {
            if (maxSeats < minSeats)
                throw new InvalidPlanMaxSeatsException(InvalidPlanMaxSeatsException.EX_MAX_SEATS_TOO_SMALL);
        }

        private void validateName(String name) throws InvalidPlanNameException {
            if (name == null)
                throw new InvalidPlanNameException(InvalidPlanNameException.EX_NAME_IS_NULL);
            if (name.length() > MAX_NAME_LENGTH)
                throw new InvalidPlanNameException(InvalidPlanNameException.EX_NAME_TOO_LONG);
            if (name.length() < MIN_NAME_LENGTH)
                throw new InvalidPlanNameException(InvalidPlanNameException.EX_NAME_TOO_SHORT);
            if (!name.matches(NAME_PATTERN))
                throw new InvalidPlanNameException(InvalidPlanNameException.EX_NAME_IS_INVALID);
        }

        private void validateDetails(String details) throws InvalidPlanDetailsException {
            if (details == null)
                throw new InvalidPlanDetailsException(InvalidPlanDetailsException.EX_DETAILS_IS_NULL);
            if (details.length() > MAX_DETAILS_LENGTH)
                throw new InvalidPlanDetailsException(InvalidPlanDetailsException.EX_DETAILS_TOO_LONG);
            if (details.length() < MIN_DETAILS_LENGTH)
                throw new InvalidPlanDetailsException(InvalidPlanDetailsException.EX_DETAILS_TOO_SHORT);
        }

        private void validateMaxSeats(int maxSeats) throws InvalidPlanMaxSeatsException {
            if (maxSeats > MAX_MAXSEATS)
                throw new InvalidPlanMaxSeatsException(InvalidPlanMaxSeatsException.EX_MAX_SEATS_TOO_LARGE);
            if (maxSeats < MIN_MAXSEATS)
                throw new InvalidPlanMaxSeatsException(InvalidPlanMaxSeatsException.EX_MAX_SEATS_TOO_SMALL);
        }

        private void validateMinSeats(int minSeats) throws InvalidPlanMinSeatsException {
            if (minSeats > MAX_MINSEATS)
                throw new InvalidPlanMinSeatsException(InvalidPlanMinSeatsException.EX_MIN_SEATS_TOO_LARGE);
            if (minSeats < MIN_MINSEATS)
                throw new InvalidPlanMinSeatsException(InvalidPlanMinSeatsException.EX_MIN_SEATS_TOO_SMALL);
        }

        private void validateFeePerUnit(int feePerUnit) throws InvalidPlanFeePerUnitException {
            if (feePerUnit > MAX_FEEPERUNIT)
                throw new InvalidPlanFeePerUnitException(InvalidPlanFeePerUnitException.EX_FEEPERUNIT_TOO_LARGE);
            if (feePerUnit < MIN_FEEPERUNIT)
                throw new InvalidPlanFeePerUnitException(InvalidPlanFeePerUnitException.EX_FEEPERUNIT_TOO_SMALL);
        }
    }


}
