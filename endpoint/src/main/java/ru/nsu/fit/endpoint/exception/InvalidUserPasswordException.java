package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class InvalidUserPasswordException extends UserException {

    public static final String EX_PSWD_TOO_SHORT = "Password is too short (must be at least 6 chars)";
    public static final String EX_PSWD_TOO_LONG = "Password is too long (must be at most 12 chars)";
    public static final String EX_PSWD_NPE = "Password is null pointer";
    public static final String EX_PSWD_CONT_FNAME = "Password must not contain first name as a substring";
    public static final String EX_PSWD_CONT_LNAME = "Password must not contain last name as a substring";
    public static final String EX_PSWD_CONT_LOGIN = "Password must not contain login as a substring";
    public static final String EX_PSWD_ONE_DIGIT = "Password must contain at least one digit";
    public static final String EX_PSWD_ONE_ALPH = "Password must contain at least one alphabetic";

    public InvalidUserPasswordException() {
        super();
    }

    public InvalidUserPasswordException(String msg) {
        super(msg);
    }

}
