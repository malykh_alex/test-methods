package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidSubscriptionUsedSeatsException extends SubscriptionException {

    public InvalidSubscriptionUsedSeatsException() {
        super();
    }

    public InvalidSubscriptionUsedSeatsException(String msg) {
        super(msg);
    }

}
