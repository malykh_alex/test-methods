package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidPlanMaxSeatsException extends PlanException {

    public static final String EX_MAX_SEATS_TOO_LARGE = "maxSeats parameter is too large";
    public static final String EX_MAX_SEATS_TOO_SMALL = "maxSeats parameter is too small";

    public InvalidPlanMaxSeatsException() {
        super();
    }

    public InvalidPlanMaxSeatsException(String msg) {
        super(msg);
    }

}
