package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidPlanNameException extends PlanException {

    public static final String EX_NAME_IS_NULL = "Name is null";
    public static final String EX_NAME_TOO_LONG = "Name parameter is too long";
    public static final String EX_NAME_TOO_SHORT = "Name parameter is too short";
    public static final String EX_NAME_IS_INVALID = "Name parameter can contain only letters and digits and must begin with a letter";

    public InvalidPlanNameException() {
        super();
    }

    public InvalidPlanNameException(String msg) {
        super(msg);
    }

}
