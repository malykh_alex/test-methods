package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class CustomerException extends IllegalArgumentException {

    public CustomerException() {
        super();
    }

    public CustomerException(String msg) {
        super(msg);
    }

}