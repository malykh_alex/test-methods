package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidSubscriptionMaxSeatsException extends SubscriptionException {

    public InvalidSubscriptionMaxSeatsException() {
        super();
    }

    public InvalidSubscriptionMaxSeatsException(String msg) {
        super(msg);
    }

}
