package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidPlanMinSeatsException extends PlanException {

    public static final String EX_MIN_SEATS_TOO_LARGE = "minSeats parameter is too large";
    public static final String EX_MIN_SEATS_TOO_SMALL = "minSeats parameter is too small";

    public InvalidPlanMinSeatsException() {
        super();
    }

    public InvalidPlanMinSeatsException(String msg) {
        super(msg);
    }

}
