package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class SubscriptionException extends IllegalArgumentException {

    public SubscriptionException() {
        super();
    }

    public SubscriptionException(String msg) {
        super(msg);
    }

}
