package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.fit.endpoint.exception.*;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User extends Entity<User.UserData> {

    public UUID getId() {
        return id;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    private UUID customerId;

    public UUID[] getSubscriptionIds() {
        return subscriptionIds;
    }

    public void setSubscriptionIds(UUID[] subscriptionIds) {
        this.subscriptionIds = subscriptionIds;
    }

    private UUID[] subscriptionIds;
    private UUID id;

    public User(UserData data, UUID id, UUID customerId) {
        super(data);
        this.id = id;
        this.customerId = customerId;
    }

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserData {

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public UserRole getUserRole() {
            return userRole;
        }

        public void setUserRole(UserRole userRole) {
            this.userRole = userRole;
        }

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("firstName")
        private String firstName;
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("lastName")
        private String lastName;
        /* указывается в виде email, проверить email на корректность */
        @JsonProperty("login")
        private String login;
        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        @JsonProperty("password")
        private String password;
        @JsonProperty("userRole")
        private UserRole userRole;

        final int MIN_FIRST_NAME_LEN = 2;
        final int MAX_FIRST_NAME_LEN = 12;
        final int MIN_LAST_NAME_LEN = 2;
        final int MAX_LAST_NAME_LEN = 12;
        final int MIN_PSWD_LEN = 6;
        final int MAX_PSWD_LEN = 12;

        private UserData(){}

        public UserData(String firstName, String lastName, String login, String password, int balance) throws UserException {
            validate(firstName, lastName, login, password, balance);
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.password = password;
            this.userRole = userRole;
        }

        public UserData(String firstName, String lastName, String login, String password, int balance, UserRole userRole) throws UserException {
            validate(firstName, lastName, login, password, balance);
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.password = password;
            this.userRole = userRole;
        }

        private void validate(String firstName, String lastName, String login, String password, int money) throws UserException {
            validateFirstName(firstName);
            validateLastName(lastName);
            validateLogin(login);
            validatePassword(password, firstName, lastName, login);
        }

        private void validateFirstName(String firstName) throws InvalidUserFirstNameException {
            if (firstName == null) {
                throw new NullPointerException(InvalidUserFirstNameException.EX_FNAME_NPE);
            }

            if (firstName.length() < MIN_FIRST_NAME_LEN) {
                throw new InvalidUserFirstNameException(InvalidUserFirstNameException.EX_FNAME_TOO_SHORT);
            }
            if (firstName.length() > MAX_FIRST_NAME_LEN) {
                throw new InvalidUserFirstNameException(InvalidUserFirstNameException.EX_FNAME_TOO_LONG);
            }

            final String firstNamePattern = "^[A-Z][a-z]+";
            boolean isOkPattern = Pattern.matches(firstNamePattern, firstName);

            if (!isOkPattern) {
                throw new InvalidUserFirstNameException(InvalidUserFirstNameException.EX_FNAME_PATTERM);
            }
        }

        private void validateLastName(String lastName) throws InvalidUserLastNameException {
            if (lastName == null) {
                throw new NullPointerException(InvalidUserLastNameException.EX_LNAME_NPE);
            }
            if (lastName.length() < MIN_LAST_NAME_LEN) {
                throw new InvalidUserLastNameException(InvalidUserLastNameException.EX_LNAME_TOO_SHORT);
            }
            if (lastName.length() > MAX_LAST_NAME_LEN) {
                throw new InvalidUserLastNameException(InvalidUserLastNameException.EX_LNAME_TOO_LONG);
            }

            final String firstNamePattern = "^[A-Z][a-z]+";
            boolean isOkPattern = Pattern.matches(firstNamePattern, lastName);

            if (!isOkPattern) {
                throw new InvalidUserLastNameException(InvalidUserLastNameException.EX_LNAME_PATTERM);
            }
        }

        private void validateLogin(String login) throws InvalidUserLoginException {
            if (login == null) {
                throw new NullPointerException(InvalidUserLoginException.EX_LOGIN_NPE);
            }

            final String loginPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            boolean isOkPattern = Pattern.matches(loginPattern, login);

            if (!isOkPattern) {
                throw new InvalidUserLoginException(InvalidUserLoginException.EX_LOGIN_PATTERN);
            }
        }

        private void validatePassword(String password, String firstName, String lastName, String login) throws InvalidUserPasswordException {
            if (password == null) {
                throw new NullPointerException(InvalidUserPasswordException.EX_PSWD_NPE);
            }

            if (password.length() < MIN_PSWD_LEN) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_TOO_SHORT);
            }
            if (password.length() > MAX_PSWD_LEN) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_TOO_LONG);
            }

            if (password.toLowerCase().contains(firstName.toLowerCase())) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_CONT_FNAME);
            }

            if (password.toLowerCase().contains(lastName.toLowerCase())) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_CONT_LNAME);
            }

            if (password.toLowerCase().contains(login.toLowerCase())) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_CONT_LOGIN);
            }

            if (!password.matches("^.*[0-9].*$")) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_ONE_DIGIT);
            }

            if (!password.matches("^.*[a-zA-Z].*$")) {
                throw new InvalidUserPasswordException(InvalidUserPasswordException.EX_PSWD_ONE_ALPH);
            }
        }
    }


}
