package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription extends Entity<Subscription.SubscriptionData> {
    private UUID id;
    private UUID customerId;
    private UUID servicePlanId;

    public Subscription(SubscriptionData data, UUID id, UUID customerId, UUID servicePlanId) {
        super(data);
        this.id = id;
        this.customerId = customerId;
        this.servicePlanId = servicePlanId;
    }

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public UUID getCustomerId() {
        return customerId;
    }
    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }
    public UUID getServicePlanId() {
        return servicePlanId;
    }
    public void setServicePlanId(UUID servicePlanId) {
        this.servicePlanId = servicePlanId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SubscriptionData {
        private SubscriptionData(){}

        public SubscriptionData(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        @JsonProperty("status")
        String status;

        public int getUsedSeats() {
            return usedSeats;
        }

        public void setUsedSeats(int usedSeats) {
            this.usedSeats = usedSeats;
        }

        @JsonProperty("usedSeats")
        private int usedSeats;
    }

}
