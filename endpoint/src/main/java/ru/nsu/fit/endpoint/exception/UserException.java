package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class UserException extends IllegalArgumentException {

    public UserException() {
        super();
    }

    public UserException(String msg) {
        super(msg);
    }

}
