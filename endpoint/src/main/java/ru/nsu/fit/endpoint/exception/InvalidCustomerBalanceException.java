package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class InvalidCustomerBalanceException extends CustomerException {

    public static final String EX_BAL_NEG = "Balance cannot be negative";

    public InvalidCustomerBalanceException() {
        super();
    }

    public InvalidCustomerBalanceException(String msg) {
        super(msg);
    }

}
