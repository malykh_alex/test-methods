package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidPlanFeePerUnitException extends PlanException {

    public static final String EX_FEEPERUNIT_TOO_LARGE = "feePerUnit parameter is too large";
    public static final String EX_FEEPERUNIT_TOO_SMALL = "feePerUnit parameter is too small";

    public InvalidPlanFeePerUnitException() {
        super();
    }

    public InvalidPlanFeePerUnitException(String msg) {
        super(msg);
    }

}
