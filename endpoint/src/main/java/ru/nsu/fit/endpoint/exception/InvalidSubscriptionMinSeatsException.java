package ru.nsu.fit.endpoint.exception;

/**
 * Created by Eugene on 24.09.2016.
 */
public class InvalidSubscriptionMinSeatsException extends SubscriptionException {

    public InvalidSubscriptionMinSeatsException() {
        super();
    }

    public InvalidSubscriptionMinSeatsException(String msg) {
        super(msg);
    }

}
