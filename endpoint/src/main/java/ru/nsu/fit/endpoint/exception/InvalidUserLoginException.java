package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 30.09.2016.
 */
public class InvalidUserLoginException extends UserException {

    public static final String EX_LOGIN_NPE = "Login is null pointer";
    public static final String EX_LOGIN_PATTERN = "Login does not math e-mail pattern";

    public InvalidUserLoginException() {
        super();
    }

    public InvalidUserLoginException(String msg) {
        super(msg);
    }

}
