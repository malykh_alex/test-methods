package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class InvalidCustomerLoginException extends CustomerException {

    public static final String EX_LOGIN_NPE = "Login is null pointer";
    public static final String EX_LOGIN_PATTERN = "Login does not math e-mail pattern";

    public InvalidCustomerLoginException() {
        super();
    }

    public InvalidCustomerLoginException(String msg) {
        super(msg);
    }

}
