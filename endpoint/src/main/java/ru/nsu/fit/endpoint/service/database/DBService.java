package ru.nsu.fit.endpoint.service.database;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.exception.CustomerException;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import javax.ws.rs.core.Response;
import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.Exchanger;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pswd, money) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_PLAN_BY_ID = "SELECT * FROM PLAN WHERE id='%s'";
    private static final String SELECT_CUSTOMER_PLAN_ID = "SELECT * FROM PLAN WHERE id='%s'";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id, used_seats, status) VALUES ('%s', '%s', '%s', '%s', '%s')";
    private static final String INSERT_USER = "INSERT INTO USER(id, customer_id, first_name, last_name, login, pswd, user_role) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String DELETE_USER = "DELETE FROM USER WHERE id='%s'";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM USER WHERE id='%s'";
    private static final String SELECT_USER_SUBSCRIPTIONS_BY_ID = "SELECT subscription_id FROM USER_ASSIGNMENT WHERE user_id='%s'";
    private static final String INSERT_USER_SUBS = "INSERT INTO USER_ASSIGNMENT(user_id, subscription_id) VALUES ('%s', '%s')";
    private static final String SELECT_SUB_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE id='%s'";
    private static final String DELETE_USER_SUB = "DELETE FROM USER_ASSIGNMENT WHERE user_id='%s' AND subscription_id='%s'";

    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");
    private static final Object generalMutex = new Object();
    private static Connection connection;

    static {
        init();
    }

    public static void createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPswd(),
                                customer.getData().getMoney()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }

    public static Customer.CustomerData getCustomerDataById(UUID customerId) throws SQLException {
        synchronized (generalMutex) {

            logger.info("Try to get customer data");

            try {
                Statement statement = connection.createStatement();
                String q = String.format(SELECT_CUSTOMER_BY_ID, customerId.toString());
                ResultSet res = statement.executeQuery(q);
                if (res.next()) {
                    UUID id = UUID.fromString(res.getString("id"));
                    String firstName = res.getString("first_name");
                    String lastName = res.getString("last_name");
                    String login = res.getString("login");
                    Integer balance = res.getInt("money");
                    String pswd = res.getString("pswd");

                    logger.info("Got customer data!");
                    return new Customer.CustomerData(firstName, lastName, login, pswd, balance);
                }
                else {
                    throw new IllegalArgumentException("No such customer!");
                }
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static Plan.PlanData getPlanDataById(UUID planId) throws SQLException {
        synchronized (generalMutex) {
            logger.info("Try to get Plan by id");

            try {
                Statement statement = connection.createStatement();
                String q = String.format(SELECT_PLAN_BY_ID, planId.toString());
                ResultSet rs = statement.executeQuery(q);
                if (rs.next()) {
                    UUID id = UUID.fromString(rs.getString("id"));
                    String name = rs.getString("name");
                    String details = rs.getString("details");
                    Integer minSeats = rs.getInt("min_seats");
                    Integer maxSeats = rs.getInt("max_seats");
                    Integer fee = rs.getInt("fee_per_seat");
                    Integer cost = rs.getInt("cost");

                    return new Plan.PlanData(name, details, maxSeats, minSeats, fee, cost);
                }
                else {
                    throw new IllegalArgumentException("No such plan!");
                }
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static boolean hasSuchPlan(UUID customerId, UUID planId) throws SQLException {
        synchronized(generalMutex) {
            logger.info("Checking if has such plan");

            try{
                Statement statement = connection.createStatement();
                String q = String.format(SELECT_CUSTOMER_PLAN_ID, customerId.toString(), planId.toString());
                ResultSet rs = statement.executeQuery(q);
                return rs.next();
            } catch(Exception ex){
                throw ex;
            }
        }
    }

    public static void updateCustomerBalance(UUID customerId, Integer val) {
        synchronized (generalMutex) {
            logger.info("Try to update money on Customer by id");

            try {
                Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
                String q = String.format(SELECT_CUSTOMER_BY_ID, customerId.toString());
                ResultSet rs = statement.executeQuery(q);
                if (rs.next()) {
                    int t = rs.getInt("money") + val;
                    if (t < 0) {
                        throw new IllegalArgumentException("Incorrect amount of money!");
                    }
                    rs.updateInt("money", t);
                    rs.updateRow();
                }
                else {
                    throw new IllegalArgumentException("No such customer!");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void tryBuyPlan(UUID customerId, UUID planId) throws SQLException {
        final String ERR_NOT_ENOUGH_MONEY = "Not enough balance to buy this plan";
        final String ERR_HAS_SUCH_PLAN = "This customer already has such plan";
        synchronized (generalMutex) {
            logger.info("Try to buy plan");

            try {
                Customer.CustomerData cd = getCustomerDataById(customerId);
                Plan.PlanData pd = getPlanDataById(planId);

                if (cd.getMoney() <= pd.getCost()) {
                    logger.debug("Not enough money");
                    throw new IllegalArgumentException(ERR_NOT_ENOUGH_MONEY);
                }

                if (hasSuchPlan(customerId, planId)) {
                    logger.debug("Has such plan");
                    throw new IllegalArgumentException(ERR_HAS_SUCH_PLAN);
                }

                boolean isExternal = pd.getName().charAt(0) == 'e';

                logger.info("Try to create subscription");

                String status = "p";
                if (isExternal) status = "d";
                Subscription subscription = new Subscription(new Subscription.SubscriptionData(status), UUID.randomUUID(), customerId, planId);

                if (isExternal) {
                    throw new NotImplementedException();
                } else {
                    Statement statement = connection.createStatement();
                    String q = String.format(INSERT_SUBSCRIPTION, subscription.getId(), subscription.getCustomerId(), subscription.getServicePlanId(),
                            0, subscription.getData().getStatus());
                    statement.executeUpdate(q);
                }

                updateCustomerBalance(customerId, -pd.getCost());

            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }

        }
    }

    public static void createUser(User.UserData userData, UUID customerId) throws SQLException {
        synchronized(generalMutex){
            logger.info("Trying to create user");

            User user = new User(userData, UUID.randomUUID(), customerId);
            try {
                Statement statement = connection.createStatement();
                String q = String.format(INSERT_USER, user.getId(), user.getCustomerId(), user.getData().getFirstName(), user.getData().getLastName(),
                        user.getData().getLogin(), user.getData().getPassword(), user.getData().getUserRole());
                statement.executeUpdate(q);
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }


    public static void deleteUser(UUID userId) throws SQLException {
        synchronized (generalMutex) {
            logger.info("Trying to delete user");

            try {
                Statement statement = connection.createStatement();
                String q = String.format(DELETE_USER, userId);
                statement.executeUpdate(q);
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static void subscribeUser(UUID customerId, UUID userId, String subscriptionId) throws SQLException {
        final int UPD_CNT = 1;
        synchronized (generalMutex) {
            logger.info("Trying to subscribe user");

            try {
                User user = getUserBy(userId);
                for (UUID id : user.getSubscriptionIds()) {
                    if (id.equals(subscriptionId)) {
                        throw new IllegalArgumentException("this user already has this subscription");
                    }
                }

                Statement statement = connection.createStatement();
                String q = String.format(INSERT_USER_SUBS, userId, subscriptionId);
                statement.executeUpdate(q);
                updateSeats(subscriptionId, UPD_CNT);

            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static void unsubscribeUser(UUID userId, String subscriptionId) throws SQLException {
        final int UPD_CNT = -1;
        synchronized (generalMutex) {
            logger.info("Trying to unsubscribe user");

            try {
                boolean hasSuch = false;
                User user = getUserBy(userId);
                for (UUID id : user.getSubscriptionIds()) {
                    if (id.equals(subscriptionId)) {
                        hasSuch = true;
                    }
                }
                if (!hasSuch) {
                    throw new IllegalArgumentException("Not subscribed to such sub!!");
                }

                Statement statement = connection.createStatement();
                String q = String.format(DELETE_USER_SUB, userId, subscriptionId);
                statement.executeUpdate(q);
                updateSeats(subscriptionId, UPD_CNT);

            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    private static User getUserBy(UUID id) throws SQLException {
        synchronized (generalMutex) {
            try {
                logger.info("Trying to get user by id");

                Statement statement = connection.createStatement();
                String q = String.format(SELECT_USER_BY_ID, id.toString());
                ResultSet rs = statement.executeQuery(q);
                if (rs.next()) {
                    UUID uid = UUID.fromString(rs.getString("id"));
                    UUID customerId = UUID.fromString(rs.getString("customer_id"));
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String login = rs.getString("login");
                    String pswd = rs.getString("pswd");
                    String role = rs.getString("user_role");
                    User.UserRole userRole = null;

                    for (User.UserRole r : User.UserRole.values()) {
                        if (r.equals(role)) { //is case sensitive??
                            userRole = r;
                            break;
                        }
                    }

                    if (userRole == null) {
                        throw new IllegalArgumentException("No such role!!");
                    }

                    User user = new User(new User.UserData(firstName, lastName, login, pswd, 0, userRole), uid, customerId);

                    q = String.format(SELECT_USER_SUBSCRIPTIONS_BY_ID, id);
                    rs = statement.executeQuery(q);
                    ArrayList<UUID> subs = new ArrayList<>();
                    while (rs.next())
                        subs.add(UUID.fromString(rs.getString("subscription_id")));
                    user.setSubscriptionIds((UUID[])subs.toArray());

                    return user;
                }
                else {
                    throw new IllegalArgumentException("No such user!");
                }
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static void updateSeats(String subscriptionId, int cnt) throws SQLException {
        synchronized(generalMutex) {
            Subscription subscription = getSubscriptionById(subscriptionId);
            try {
                logger.info("Trying to update subscription seats");

                Statement statement = connection.createStatement();
                String q = String.format(SELECT_SUB_BY_ID, subscriptionId);

                ResultSet rs = statement.executeQuery(q);
                if (rs.next()) {
                    int t = rs.getInt("used_seats") + cnt;
                    Plan.PlanData pd = getPlanDataById(subscription.getServicePlanId());
                    if (t > pd.getMaxSeats() || t < pd.getMinSeats())
                        throw new IllegalArgumentException("Incorrect seats cnt!");
                    rs.updateInt("used_seats", t);
                    rs.updateRow();
                } else {
                    throw new IllegalArgumentException("No such sub!");
                }
            }catch(Exception ex){
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static Subscription getSubscriptionById(String id) throws SQLException {
        synchronized (generalMutex){
            try {
                logger.info("Trying to get sub by id");

                Statement statement = connection.createStatement();
                String q = String.format(SELECT_SUB_BY_ID, id);
                ResultSet rs = statement.executeQuery(q);
                if (rs.next()) {
                    UUID customerId = UUID.fromString(rs.getString("customer_id"));
                    UUID planId = UUID.fromString(rs.getString("plan_id"));
                    int usedSeats = rs.getInt("used_seats");
                    String status = rs.getString("status");

                    Subscription subscription = new Subscription(new Subscription.SubscriptionData(status), UUID.fromString(id), customerId, planId);
                    subscription.getData().setUsedSeats(usedSeats);

                    return subscription;
                }
                else {
                    throw new IllegalArgumentException("No such sub!!");
                }
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }

    public static void changeUserRole(String userId, String role) throws SQLException {
        synchronized (generalMutex) {
            logger.info("Try to change user role");

            try {
                Statement statement = connection.createStatement();
                String q = String.format(SELECT_USER_BY_ID, userId);
                ResultSet rs = statement.executeQuery(q);
                if(rs.next()) {
                    rs.updateString("role", role);
                    rs.updateRow();
                }
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                throw ex;
            }
        }
    }
}
