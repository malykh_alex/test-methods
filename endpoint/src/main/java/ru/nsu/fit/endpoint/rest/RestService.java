package ru.nsu.fit.endpoint.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.glassfish.jersey.internal.util.Base64;
import ru.nsu.fit.endpoint.exception.CustomerException;
import ru.nsu.fit.endpoint.exception.UserException;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("/rest")
public class RestService {

    final int STATUS_OK  = 200;
    final int STATUS_ERR = 400;

    private String getCurrentAuthenticationLogin(String authentication) {
        String s = authentication.split(" ")[1];
        String res = Base64.decodeAsString(s).split(":")[0];
        return res;
    }

    //CUSTOMER section
    //==========================================================================================================================

    @RolesAllowed("ADMIN")
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            DBService.createCustomer(customerData);
            return Response.status(STATUS_OK).entity(customerData.toString()).build();
        } catch (CustomerException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.status(STATUS_OK).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (CustomerException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({"ADMIN","CUSTOMER"})
    @GET
    @Path("/get_customer_data/{customer_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerData(@PathParam("customer_id") UUID customerId) {
        try {

            //================================================================================ Check that it's his id
            Customer.CustomerData customerData = DBService.getCustomerDataById(customerId);
            String response = JsonMapper.toJson(customerData, true);
            return Response.status(STATUS_OK).entity(response).build();
        } catch (CustomerException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/buy_plan/{plan_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buyPlanById(@HeaderParam("Authorization") String auth, @PathParam("plan_id") UUID planId) {

        final String ERR_HAS_PLAN = "This customer already has this plan";
        final String ERR_NOT_ENOUGH_MONEY = "Not enough money to continue operation";
        final String SUCCESS = "Success!";

        try {
            UUID customerId = DBService.getCustomerIdByLogin(getCurrentAuthenticationLogin(auth));
            DBService.tryBuyPlan(customerId, planId);
            return Response.status(STATUS_OK).entity(SUCCESS).build();
        } catch (CustomerException ex) {
            //if not enough money
            //or already owns this plan
            //or other ex
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/update_balance/{customer_id}/{amount}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response topUpCustomerBalance(@PathParam("customer_id") UUID customerId, @PathParam("amount") Integer amount) {
        try {
            DBService.updateCustomerBalance(customerId, amount);
            return Response.status(STATUS_OK).entity(amount + " money added to customer: " + customerId + "\n").build();
        } catch (CustomerException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    //USER section
    //==========================================================================================================================

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/create_user")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(@HeaderParam("Authorization") String auth, String jasonData){
        try {
            User.UserData userData = JsonMapper.fromJson(jasonData, User.UserData.class);
            UUID customerId = DBService.getCustomerIdByLogin(getCurrentAuthenticationLogin(auth));
            DBService.createUser(userData, customerId);
            return Response.status(STATUS_OK).entity(userData.toString()).build();
        }
        catch (CustomerException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @DELETE
    @Path("/delete_user/{user_id}")
    public Response deleteUser(@PathParam("user_id") UUID userId){
        try {
            DBService.deleteUser(userId);
            return Response.status(STATUS_OK).entity("User " + userId.toString() + " deleted").build();
        } catch (CustomerException ex){
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @PUT
    @Path("/subscribe_user/{user_id}/{subscriptionId}")
    public Response subscribeUser(@HeaderParam("Authorization") String auth, @PathParam("userId") String userId, @PathParam("subscriptionId") String subscriptionId) {
        try{
            String login = getCurrentAuthenticationLogin(auth);
            UUID customerId = DBService.getCustomerIdByLogin(login);
            DBService.subscribeUser(customerId, UUID.fromString(userId), subscriptionId);
            return Response.status(STATUS_OK).entity("User: " + userId.toString() + " was subscribed: " + subscriptionId.toString()).build();
        }catch (IllegalArgumentException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @PUT
    @Path("/unsubscribe_user/{user_id}/{subscriptionId}")
    public Response unsubscribeUser(@PathParam("userId") String userId, @PathParam("subscriptionId") String subscriptionId){
        try{
            DBService.unsubscribeUser(UUID.fromString(userId), subscriptionId);
            return Response.status(STATUS_OK).entity("Unsubscribed user: " + userId.toString() + " subscription: " + subscriptionId.toString()).build();
        }catch (IllegalArgumentException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @PUT
    @Path("/change_user_role/{user_id}/{role}")
    public Response changeUserRole(@PathParam("user_id") String userId, @PathParam("role") String role){
        try {
            DBService.changeUserRole(userId, role);
            return Response.status(STATUS_OK).entity("Changed role successfully!").build();
        } catch (CustomerException ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (Exception ex) {
            return Response.status(STATUS_ERR).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }



}