package ru.nsu.fit.endpoint.exception;

/**
 * Created by Alex on 24.09.2016.
 */
public class InvalidUserFirstNameException extends UserException {

    public static final String EX_FNAME_TOO_SHORT = "First name is too short (must be at least 2 chars)";
    public static final String EX_FNAME_TOO_LONG = "First name is too long (must be at most 12 chars)";
    public static final String EX_FNAME_NPE = "First name is null pointer";
    public static final String EX_FNAME_PATTERM = "First name does not start with the capital, contain other non-capitals or non alphabetics";

    public InvalidUserFirstNameException() {
        super();
    }

    public InvalidUserFirstNameException(String msg) {
        super(msg);
    }

}
