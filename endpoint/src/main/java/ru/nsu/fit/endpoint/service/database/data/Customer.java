package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.exception.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Customer extends Entity<Customer.CustomerData> {

    private UUID id;

    public Customer(CustomerData data, UUID id) throws CustomerException {
        super(data);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CustomerData {

        final int MIN_FIRST_NAME_LEN = 2;
        final int MAX_FIRST_NAME_LEN = 12;
        final int MIN_LAST_NAME_LEN = 2;
        final int MAX_LAST_NAME_LEN = 12;
        final int MIN_PSWD_LEN = 6;
        final int MAX_PSWD_LEN = 12;

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("firstName")
        private String firstName;

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("lastName")
        private String lastName;

        /* указывается в виде email, проверить email на корректность */
        @JsonProperty("login")
        private String login;

        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        @JsonProperty("pswd")
        private String pswd;

        private CustomerData() {}

        /* счет не может быть отрицательным */
        @JsonProperty("money")
        private int money;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPswd() {
            return pswd;
        }

        public void setPswd(String pswd) {
            this.pswd = pswd;
        }

        public int getMoney() {
            return money;
        }

        public void setMoney(int money) {
            this.money = money;
        }

        public CustomerData(String firstName, String lastName, String login, String pswd, int money) throws CustomerException {
            validate(firstName, lastName, login, pswd, money);
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.pswd = pswd;
            this.money = money;
        }

        private void validate(String firstName, String lastName, String login, String password, int money) throws CustomerException {
            validateFirstName(firstName);
            validateLastName(lastName);
            validateLogin(login);
            validatePassword(password, firstName, lastName, login);
            validateBalance(money);
        }

        private void validateFirstName(String firstName) throws InvalidCustomerFirstNameException {
            if (firstName == null) {
                throw new NullPointerException(InvalidCustomerFirstNameException.EX_FNAME_NPE);
            }

            if (firstName.length() < MIN_FIRST_NAME_LEN) {
                throw new InvalidCustomerFirstNameException(InvalidCustomerFirstNameException.EX_FNAME_TOO_SHORT);
            }
            if (firstName.length() > MAX_FIRST_NAME_LEN) {
                throw new InvalidCustomerFirstNameException(InvalidCustomerFirstNameException.EX_FNAME_TOO_LONG);
            }

            final String firstNamePattern = "^[A-Z][a-z]+";
            boolean isOkPattern = Pattern.matches(firstNamePattern, firstName);

            if (!isOkPattern) {
                throw new InvalidCustomerFirstNameException(InvalidCustomerFirstNameException.EX_FNAME_PATTERM);
            }
        }

        private void validateLastName(String lastName) throws InvalidCustomerLastNameException {
            if (lastName == null) {
                throw new NullPointerException(InvalidCustomerLastNameException.EX_LNAME_NPE);
            }
            if (lastName.length() < MIN_LAST_NAME_LEN) {
                throw new InvalidCustomerLastNameException(InvalidCustomerLastNameException.EX_LNAME_TOO_SHORT);
            }
            if (lastName.length() > MAX_LAST_NAME_LEN) {
                throw new InvalidCustomerLastNameException(InvalidCustomerLastNameException.EX_LNAME_TOO_LONG);
            }

            final String firstNamePattern = "^[A-Z][a-z]+";
            boolean isOkPattern = Pattern.matches(firstNamePattern, lastName);

            if (!isOkPattern) {
                throw new InvalidCustomerLastNameException(InvalidCustomerLastNameException.EX_LNAME_PATTERM);
            }
        }

        private void validateLogin(String login) throws InvalidCustomerLoginException {
            if (login == null) {
                throw new NullPointerException(InvalidCustomerLoginException.EX_LOGIN_NPE);
            }

            final String loginPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            boolean isOkPattern = Pattern.matches(loginPattern, login);

            if (!isOkPattern) {
                throw new InvalidCustomerLoginException(InvalidCustomerLoginException.EX_LOGIN_PATTERN);
            }
        }

        private void validatePassword(String password, String firstName, String lastName, String login) throws InvalidCustomerPasswordException {
            if (password == null) {
                throw new NullPointerException(InvalidCustomerPasswordException.EX_PSWD_NPE);
            }

            if (password.length() < MIN_PSWD_LEN) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_TOO_SHORT);
            }
            if (password.length() > MAX_PSWD_LEN) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_TOO_LONG);
            }

            if (password.toLowerCase().contains(firstName.toLowerCase())) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_CONT_FNAME);
            }

            if (password.toLowerCase().contains(lastName.toLowerCase())) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_CONT_LNAME);
            }

            if (password.toLowerCase().contains(login.toLowerCase())) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_CONT_LOGIN);
            }

            if (!password.matches("^.*[0-9].*$")) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_ONE_DIGIT);
            }

            if (!password.matches("^.*[a-zA-Z].*$")) {
                throw new InvalidCustomerPasswordException(InvalidCustomerPasswordException.EX_PSWD_ONE_ALPH);
            }
        }

        private void validateBalance(int balance) throws InvalidCustomerBalanceException {
            if (balance < 0) {
                throw new InvalidCustomerBalanceException(InvalidCustomerBalanceException.EX_BAL_NEG);
            }
        }
    }

}
